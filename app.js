// Iteracion #1 - Usa includes

const products = ['Camiseta de Pokemon', 'Pantalón coquinero', 'Gorra de gansta', 'Camiseta de Basket', 'Cinrurón de Orión', 'AC/DC Camiseta'];
products.forEach(prod => {
    if (prod.includes('Camiseta')) {
        console.log(prod);
    }
});

// Iteracion #2 - Condicionales avanzados

const alumns = [
    { name: 'Pepe Viruela', T1: false, T2: false, T3: true },
    { name: 'Lucia Aranda', T1: true, T2: false, T3: true },
    { name: 'Juan Miranda', T1: false, T2: true, T3: true },
    { name: 'Alfredo Blanco', T1: false, T2: false, T3: false },
    { name: 'Raquel Benito', T1: true, T2: true, T3: true },
];

alumns.forEach(alumn => {
    if ((alumn.T1 == false && alumn.T2 == false) || (alumn.T2 == false && alumn.T3 == false) || (alumn.T1 == false && alumn.T3 == false)) {
        alumn.isApproved = false;
    } else {
        alumn.isApproved = true;
    }
});

console.log(alumns);

// Iteracion #3 - Probando For...of

const placesToTravel = ['Japon', 'Venecia', 'Murcia', 'Santander', 'Filipinas', 'Madagascar'];
for (const place of placesToTravel) {
    console.log(place);
}

// Iteracion #4 - Probando For...in

const alien = {
    name: 'Wormuck',
    race: 'Cucusumusu',
    planet: 'Eden',
    weight: '259kg',
};

for (const prop in alien) {
    console.log(`${prop}: ${alien[prop]}`);
}

// Iteracion #5 - Probando For

const placesToTravel2 = [
    { id: 5, name: 'Japan' },
    { id: 11, name: 'Venecia' },
    { id: 23, name: 'Murcia' },
    { id: 40, name: 'Santander' },
    { id: 44, name: 'Filipinas' },
    { id: 59, name: 'Madagascar' },
];

for (let i = 0; i < placesToTravel2.length; i++) {
    if (placesToTravel2[i].id == 11 || placesToTravel2[i].id == 40) {
        console.log(placesToTravel2[i]);
    }
}

// Iteracion #6 - Mixed For...of e includes

const toys = [
    { id: 5, name: 'Buzz MyYear' },
    { id: 11, name: 'Action Woman' },
    { id: 23, name: 'Barbie Man' },
    { id: 40, name: 'El gato con Guantes' },
    { id: 40, name: 'El gato felix' },
];

// FOR...OF JUST DELETES THE FIRST ONE

for (const toy of toys) {
    var toyIndex = toys.indexOf(toy);
    if (toy.name.includes('gato')) {
        toys.splice(toyIndex, 1);
    }
}
console.log(toys);

// FOR...EACH WORKS FINE

toys.forEach(toy => {
    var toyIndex = toys.indexOf(toy);
    if (toy.name.includes('gato')) {
        toys.splice(toyIndex, 1);
    }
});
console.log(toys);

// Iteracion #7 - For...of avanzado

const popularToys = [];
const toys2 = [
    { id: 5, name: 'Buzz MyYear', sellCount: 10 },
    { id: 11, name: 'Action Woman', sellCount: 24 },
    { id: 23, name: 'Barbie Man', sellCount: 15 },
    { id: 40, name: 'El gato con Guantes', sellCount: 8 },
    { id: 40, name: 'El gato felix', sellCount: 35 },
];

for (const toy of toys2) {
    var toyIndex = toys2.indexOf(toy);
    if (toy.sellCount > 15) {
        popularToys.push(toy);
    }
}
console.log(popularToys);
